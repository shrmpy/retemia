package red

import (
	"bytes"
	"context"
	"testing"

	"github.com/gobwas/ws"
)

var bg = context.Background()

func TestRecv(t *testing.T) {
	var remote Gateway
	remote = &Red{}
	exp := helloData()
	rdr := bytes.NewReader(helloBytes(t))

	// Actual logic that we want to test
	evt, err := remote.Recv(bg, rdr)
	if err != nil {
		t.Errorf("unexpected error: %v; want %v", err, nil)
	}

	if evt.Opcode() != exp.Opcode() {
		t.Errorf(
			"unexpected message op code: %v; want %v",
			evt.Opcode(), exp.Opcode(),
		)
	}
	if evt.Opcode() != GWOP_HELLO {
		t.Errorf(
			"unexpected message op code: %v; want %v",
			evt.Opcode(), GWOP_HELLO,
		)
	}

	gh := evt.(GatewayHello)

	if gh.EventData.HeartbeatMillisec != exp.EventData.HeartbeatMillisec {
		t.Errorf(
			"unexpected heartbeat ms: %v; want %v",
			gh.EventData.HeartbeatMillisec,
			exp.EventData.HeartbeatMillisec,
		)
	}
}

// helloData is the rehydrated struct result that we expect
func helloData() GatewayHello {
	evt := GatewayHello{}
	evt.Op = GWOP_HELLO
	evt.EventData.HeartbeatMillisec = 45000
	evt.EventData.Trace = []string{"discord-gateway-prd-1-99"}
	return evt
}

// helloBytes is the fake WebSocket data of Op-10 (would be sent by the gateway)
// example: {"t":null,"s":null,"op":10,"d":{"heartbeat_interval":41250,"_trace":["gateway-prd-main-wd86"]}}
func helloBytes(t *testing.T) []byte {
	buf := &bytes.Buffer{}
	fs := []ws.Frame{
		ws.NewFrame(ws.OpText, false, []byte(`{"t":null,"s":null,"op":10,`)),
		ws.NewFrame(ws.OpContinuation, false, []byte(`"d":{`)),
		ws.NewFrame(ws.OpContinuation, false, []byte(`"heartbeat_interval":45000,`)),
		ws.NewFrame(ws.OpContinuation, false, []byte(`"_trace":["discord-gateway-prd-1-99"]`)),
		ws.NewFrame(ws.OpContinuation, true, []byte(`}}`)),
	}
	for _, f := range fs {
		if err := ws.WriteFrame(buf, f); err != nil {
			t.Fatal(err)
		}
	}
	return buf.Bytes()
}
