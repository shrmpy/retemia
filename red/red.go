package red

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net"
	"time"

	"github.com/gobwas/ws/wsutil"
)

const (
	DISCORD_TOKEN = "DISCORDTOKEN"
	GW_VERSION    = 6
	GW_ENCODING   = "json"
	GW_COMPRESS   = false
	GWOP_BEAT     = 1
	GWOP_HELLO    = 10
	GWOP_BEATACK  = 11
)

type Red struct {
	Base         string
	seqnum       int
	hasHeartbeat bool
	ticker       *time.Ticker
	resume       *time.Timer
	rcv          queue
}

// Call workers responsible for the gateway event
func (r *Red) StartWorkers(ctx context.Context, conn net.Conn, evt Payload) {

	switch evt.Opcode() {
	case GWOP_BEAT:
		sendHeartbeat(conn, r.seqnum)

	case GWOP_HELLO:
		gh := evt.(GatewayHello)
		r.saveSequenceNum(gh)
		cx1 := context.WithValue(ctx, "seq", r.seqnum)
		cx2 := context.WithValue(cx1, "hbms", gh.EventData.HeartbeatMillisec)
		r.Keepalive(cx2, conn)

		// TODO Send-Identify-Request
		log.Println("TODO: identify job")

	case GWOP_BEATACK:
		r.resume.Stop()
		log.Println("RCVD Heartbeat ACK")
	}
}

func (r *Red) Keepalive(ctx context.Context, conn net.Conn) error {
	if r.hasHeartbeat {
		// Already sending heartbeat
		return nil
	}
	r.hasHeartbeat = true

	interval, ok := ctx.Value("hbms").(int)
	if !ok {
		return fmt.Errorf("Heartbeat interval not found: %v", ctx)
	}
	dur := time.Duration(interval) * time.Millisecond

	// DEBUG DEBUG
	log.Println("Starting heartbeat ticker: ", dur)

	r.ticker = time.NewTicker(dur)
	needResume := make(chan error, 1)
	go func() {
		for {
			select {
			case <-ctx.Done():
				// Halt signal
				needResume <- ctx.Err()
			case <-needResume:
				// Teardown heartbeat job (to attempt resume).
				r.ticker.Stop()
				close(needResume)
				r.hasHeartbeat = false
				return
			case <-r.ticker.C:
				if seq, ok := ctx.Value("seq").(int); ok {
					if err := sendHeartbeat(conn, seq); err != nil {
						needResume <- err
					} else {
						// Start a timeout on ACK wait.
						r.resume = time.AfterFunc(dur, func() {
							needResume <- fmt.Errorf("Missing Heartbeat ACK")
						})
					}
				}
			}
		}
	}()
	return nil
}

// sendHeartbeat transmits the Op-1 to the gateway
// which must be done every heartbeat_interval and
// when the gateway initiates with Op-1
func sendHeartbeat(conn net.Conn, sequence int) error {
	var err error
	var buf []byte
	var req = struct {
		Opcode    int  `json:"op"`
		EventData *int `json:"d"`
	}{Opcode: 1}

	if sequence == 0 {
		req.EventData = nil
	} else {
		req.EventData = &sequence
	}
	buf, err = json.Marshal(req)
	if err != nil {
		return fmt.Errorf("ERROR marshal heartbeat: %v", err)
	}

	// DEBUG DEBUG
	log.Println("Sending heartbeat: ", string(buf))

	err = wsutil.WriteClientText(conn, buf)
	if err != nil {
		return fmt.Errorf("ERROR WebSocket heartbeat: %v", err)
	}
	return nil
}

// saveSequenceNum keeps number to be used in Op-1
func (r *Red) saveSequenceNum(payload GatewayHello) {
	if payload.SequenceNum() != 0 {
		r.seqnum = payload.SequenceNum()
	}
}

// Opcode extracts opcode from the gateway message payload
func Opcode(data []byte) int {
	evt := Event(data)
	m := evt.(map[string]interface{})

	if op, ok := m["op"]; ok {
		n := op.(float64)
		return int(n)
	}
	log.Fatal("ERROR JSON field: op ")
	return 0
}

// HelloEvent unmarshals bytes to hello event
func HelloEvent(data []byte) GatewayHello {
	evt := GatewayHello{}
	jsonErr := json.Unmarshal(data, &evt)
	if jsonErr != nil {
		log.Fatal(jsonErr)
	}
	return evt
}

// Event unmarshals bytes before type is known
func Event(data []byte) interface{} {
	var evt interface{}
	err := json.Unmarshal(data, &evt)
	if err != nil {
		log.Fatal(err)
	}
	return evt
}

// Connect makes WebSocket connection to the gateway
func (r *Red) Connect(ctx context.Context, gatewayRequest GatewayRequest) (net.Conn, error) {
	// Using the wsutil test logic as reference
	// even if DebugDialer is slower.

	dd := wsutil.DebugDialer{OnResponse: debugrcv, OnRequest: debugreq}

	pt := gatewayRequest.URL(r.Base)

	conn, br, _, err := dd.Dial(ctx, pt)
	if err != nil {
		return nil, fmt.Errorf("WebSocket connect error: %v", err)
	}

	if br != nil {
		body, err := ioutil.ReadAll(br)

		if err != nil {
			return nil, fmt.Errorf("WebSocket immediate data error: %v", err)
		}
		// DEBUG DEBUG
		log.Println("Immediate data from connect: ", body)
	}
	return conn, nil
}

func debugreq(data []byte) {
	log.Println("REQ: ", string(data))
}
func debugrcv(data []byte) {
	log.Println("RCV: ", string(data))
}

func (r *Red) Stop() {
	if r.hasHeartbeat {
		r.ticker.Stop()
	}
}
