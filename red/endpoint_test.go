package red

import (
	"encoding/json"
	"log"
	"testing"
)

const URL_VALUE = `http://free.beer/`

type fakeWebRequest struct{}

func (fakeWebRequest) FetchBytes(url string) []byte {
	d := EndpointData{URL: URL_VALUE}
	log.Printf("Test input: %v\n", d)

	buf, err := json.Marshal(d)
	if err != nil {
		log.Println("Test marshal error: ", err)
		return nil
	}

	log.Println("Test input bytes: ", string(buf))
	return buf
}

// Exercise the Endpoint implementation to check
// gateway response value is equivalent to input value
func TestEndpoint(t *testing.T) {
	// The input that we control by pre-determining the field value.
	input := fakeWebRequest{}

	// Actual Endpoint logic that we want to test.
	r := Red{}
	val, _ := r.Endpoint(input)

	if val != URL_VALUE {
		t.Errorf("Endpoint FAILED -- expected: %s, received: %s", URL_VALUE, val)
	}
}
