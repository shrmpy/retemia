package red

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"time"
)

const (
	AGENT_NAME = "retemia"
	AGENT_URL  = "https://github.com/shrmpy/retemia/"
	AGENT_VER  = "0.1.0"
)

// Endpoint obtains the gateway WebSocket URL
// which can be useful for the resume flow
func (r *Red) Endpoint(webRequest WebRequest) (string, error) {
	log.Println("New request: ", webRequest)
	api := "https://discordapp.com/api/gateway"
	log.Println("API: ", api)
	bodyBytes := webRequest.FetchBytes(api)
	log.Println("Bytes fetched: ", string(bodyBytes))
	gatewayResult := EndpointData{}
	log.Println("Unmarshal bytes")
	jsonErr := json.Unmarshal(bodyBytes, &gatewayResult)
	if jsonErr != nil {
		return "", jsonErr
	}
	log.Println("Json: ", gatewayResult)
	return gatewayResult.URL, nil
}

type EndpointData struct {
	URL string `json:"url"`
}

func (w LiveWebRequest) FetchBytes(url string) []byte {
	webClient := http.Client{
		Timeout: time.Second * 2,
	}
	req, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		log.Fatal(err)
	}

	req.Header.Set("User-Agent", w.agentHeader())
	req.Header.Set("Authorization", w.tokenHeader())

	res, getErr := webClient.Do(req)
	if getErr != nil {
		log.Fatal(getErr)
	}

	body, readErr := ioutil.ReadAll(res.Body)
	if readErr != nil {
		log.Fatal(readErr)
	}

	return body
}

func (w LiveWebRequest) agentHeader() string {
	return fmt.Sprintf("%s (%s, %s)", w.Agent, w.Agenturl, w.Agentver)
}
func (w LiveWebRequest) tokenHeader() string {
	return fmt.Sprintf("Bot %s", w.Token)
}

type LiveWebRequest struct {
	Token    string
	Agent    string
	Agenturl string
	Agentver string
}

type WebRequest interface {
	FetchBytes(url string) []byte
}
