package red

import (
	"context"
	"fmt"
	"io"
	"net"
)

type Gateway interface {
	Endpoint(WebRequest) (string, error)
	Connect(context.Context, GatewayRequest) (net.Conn, error)
	Recv(context.Context, io.Reader) (Payload, error)
	Keepalive(context.Context, net.Conn) error
}

type Payload interface {
	Opcode() int
}

type GatewayEvent struct {
	Event interface{} `json:"t"`
	Seq   interface{} `json:"s"`
	Op    int         `json:"op"`
}

func (e GatewayEvent) Opcode() int {
	return e.Op
}
func (e GatewayEvent) EventName() string {
	if e.Event == nil {
		return ""
	}
	return e.Event.(string)
}

type GatewayHello struct {
	GatewayEvent
	EventData struct {
		HeartbeatMillisec int      `json:"heartbeat_interval"`
		Trace             []string `json:"_trace"`
	} `json:"d"`
}

func (e GatewayHello) Opcode() int {
	return e.Op
}
func (e GatewayHello) SequenceNum() int {
	if e.Seq == nil {
		return 0
	}
	return e.Seq.(int)
}

type GatewayIdentify struct {
	GatewayEvent
	eventData struct {
		Token          string `json:"token"`
		Properties     string `json:"properties"`
		Compress       bool   `json:"compress"`
		LargeThreshold int    `json:"large_threshold"`
	} `json:"d"`
}

type GatewayRequest struct {
	Token    string
	Compress bool
	Encoding string
	V        int
}

func (r GatewayRequest) URL(base string) string {
	url := fmt.Sprintf("%s?v=%d&encoding=%s", base, r.V, r.Encoding)
	return url
}
