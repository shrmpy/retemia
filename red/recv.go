package red

import (
	"context"
	"fmt"
	"io"
	"log"

	"github.com/gobwas/ws"
	"github.com/gobwas/ws/wsutil"
)

func (r *Red) Recv(ctx context.Context, rdr io.Reader) (Payload, error) {
	if r.rcv.Size() == 0 {

		var ls []wsutil.Message
		messages, err := wsutil.ReadServerMessage(rdr, ls)
		if err != nil {
			return GatewayEvent{}, err
		}

		for _, v := range messages {
			if v.OpCode == ws.OpText {
				// Only use text WebSocket data
				r.rcv.Enqueue(v)
			}
		}
	}
	if r.rcv.Size() == 0 {
		// Still nothing, warn caller.
		return GatewayEvent{}, fmt.Errorf("Empty queue, wait for new data")
	}

	msg := r.rcv.Dequeue()
	return unmarshalEvent(msg), nil
}

func unmarshalEvent(msg wsutil.Message) Payload {
	op := Opcode(msg.Payload)

	switch op {
	case GWOP_HELLO:
		return HelloEvent(msg.Payload)
	default:
		// TODO not used yet
		return GatewayEvent{Op: op}
	}
}

type queue struct {
	data []wsutil.Message
}

func newQueue() *queue {
	return &queue{data: []wsutil.Message{}}
}

func (s *queue) Enqueue(item wsutil.Message) {
	s.data = append(s.data, item)
}

func (s *queue) Dequeue() (datum wsutil.Message) {
	n := s.Size()
	switch n {
	case 0:
		log.Fatalf("ERROR Dequeue on zero queue: %v", s)
	case 1:
		datum = s.data[0]
		s.Clear()
	default:
		datum, s.data = s.data[0], s.data[1:n-1]
	}
	return
}

func (s *queue) Size() int {
	return len(s.data)
}

func (s *queue) Clear() {
	s.data = []wsutil.Message{}
}
