package main

import (
	"context"
	"fmt"
	"log"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/shrmpy/retemia/red"
)

func main() {
	wrq := red.LiveWebRequest{
		Token:    os.Getenv(red.DISCORD_TOKEN),
		Agent:    red.AGENT_NAME,
		Agenturl: red.AGENT_URL,
		Agentver: red.AGENT_VER,
	}

	rem := &red.Red{}

	pt, err := rem.Endpoint(wrq)
	if err != nil {
		log.Fatalf("ERROR Endpoint: %v", err)
	}
	rem.Base = pt

	ctx, cancel := context.WithCancel(context.Background())
	grq := red.GatewayRequest{
		Token:    os.Getenv(red.DISCORD_TOKEN),
		Compress: red.GW_COMPRESS,
		Encoding: red.GW_ENCODING,
		V:        red.GW_VERSION,
	}
	conn, err := rem.Connect(ctx, grq)
	if err != nil {
		log.Fatalf("ERROR WebSocket connect failed: %v", err)
	}
	defer conn.Close()
	errc := make(chan error)

	// Interrupt handler.
	go func() {
		c := make(chan os.Signal, 1)
		signal.Notify(c, syscall.SIGINT, syscall.SIGTERM)
		errc <- fmt.Errorf("%s", <-c)
	}()

	// Pause 300ms between WebSocket data.
	poll := time.NewTicker(300 * time.Millisecond)

	for {
		select {
		case err := <-errc:
			poll.Stop()
			rem.Stop()
			close(errc)
			cancel()
			log.Println("Shutdown: ", err)
			return

		case <-poll.C:
			if m, err := rem.Recv(ctx, conn); err != nil {
				errc <- err
			} else {
				rem.StartWorkers(ctx, conn, m)
			}
		}
	}
}
